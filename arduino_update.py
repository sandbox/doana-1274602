#!/usr/bin/python

#This is really just a wrapper for curl that pulls the page from the drupal site and writes it to the serial port. 

import serial
import pycurl

#Update these two variables to reflect your local configuration. 
drupal_url = 'http://example.com/arduino_update/[key]/update.txt'
arduino_dev = '/dev/ttyACM0'

def on_receive(data):
	ser = serial.Serial(arduino_dev, 9600)
	ser.write(data)
	ser.close()

c = pycurl.Curl()
c.setopt(c.URL, drupal_url)
c.setopt(pycurl.WRITEFUNCTION, on_receive) 
c.perform()
c.close()
