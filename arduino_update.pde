/*
 * Listens on the serial connection for a string in the format "x:y\n" where x and y can take the values of 0 and 1. 
 * When a 1 is passed for x or y, the arduino responds by flashing the corresponding pins on and off.
 * The x value corresponds to the core pin (pin 10) whereas the y value corresponds to the contrib pin (pin 9).
 * 
 * On linux, do this to configure tty to enable manual communication with the arduino:
 * stty -F /dev/ttyACM0 cs8 9600 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts 
 * Where /dev/ttyACM0 is the device that your arduino is connected to. 
 * Note that 9600 is the baud rate of the connection. These must agree between the computer and the arduino or you'll get gibberish.  
 * Then do this to send a command to the arduino: 
 * echo "0:0" > /dev/ttyACM0
*/

String input = "";

int corePin = 10;
int contribPin = 9;

boolean coreFlag = false;
boolean contribFlag = false;

void setup() {
  Serial.begin(9600); 
  pinMode(corePin, OUTPUT);
  pinMode(contribPin, OUTPUT);  
}

void loop() {
    //Read from the serial connection until we have 4 characters. Then parse the input and respond accordingly. 
	if(Serial.available()) {
		char ch = Serial.read();
		input += ch;
    }
    
    if(input.length() == 4) {
        Serial.println(input);
        parseInput(input);
        input = "";
    }
    
    if(coreFlag && !contribFlag) {
        flash(corePin, 300);
    }
    else if(!coreFlag && contribFlag) {
        flash(contribPin, 300);
    }
    else if(coreFlag && contribFlag) {
        alternate(corePin, contribPin, 300);
    }
    else if(!coreFlag && !contribFlag) {
        reset(corePin, contribPin); 
    }
}
/*
 * Parse the input string and set the appropriate flags. We're looking for 'x:y\n' as our string. 
 */
void parseInput(String input) {
    //Make sure we have the '\n' at the end. If we don't, read from the serial port until we do to sync the stream.
    if(input[3] != 10) {
        char ch;
        do {
            ch = Serial.read();
        } while(Serial.available() && ch != 10); 
    }
    
    if(input[0] == '1') {
        coreFlag = true;
    }
    else {
        coreFlag = false;
    }
  
    if(input[2] == '1') {
        contribFlag = true;
    }
    else {
        contribFlag = false;
    }
}

/*
 * Flash a pin on and off.
 */
void flash(int pin, int dur) {
  digitalWrite(pin, HIGH);
  delay(dur);
  digitalWrite(pin, LOW);
  delay(dur);
}

/*
 * Alternate between two pins.  
 */
void alternate(int pin1, int pin2, int dur) {
  digitalWrite(pin1, HIGH);
  digitalWrite(pin2, LOW);
  delay(dur);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, HIGH);
  delay(dur);
}

/*
 * Turn both pins off. 
 */
void reset(int pin1, int pin2) {
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW); 
}
