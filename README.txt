To install this module and get it working with your arduino. These instructions were created for ubuntu linux. 

Step 1: Install the drupal module
Step 2: Go to admin/config/system/arduino_update and copy the unique url provided on that page. 
Step 3: Edit the .py file and update the drupal_url variable with the value obtained from your site. 
Step 4: Set up your arduino by connecting an LED or other device to pins 9 and 10 of your arduino. Make sure you use a resistor if necessary. 
		Connect your arduino to the client computer via USB, and upload the sketch to the arduino. At this stage, verify the device that your system is using to communicate with the arduino. If necessary, update the python file to reflect the correct value. 
Step 5: Put the python file somewhere on your client machine that the arduino will be connected to. Make it executable by issuing the command 'sudo chmod +x arduino_update.py'
Step 5: Add a line to your crontab so that the python file is executed periodically: On linux your home directory is probably a good choice (/home/username/bin/arduino_update/arduino_update.py).
		Run 'sudo crontab -e'
		Add something resembling the following to the end of the file and save it:
		* * * * *       /home/username/bin/arduino_update/arduino_update.py 
Step 6: Profit!
	
